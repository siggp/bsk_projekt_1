﻿using NUnit.Framework;
using System.IO;

namespace Tests
{
	class FileInfoTest
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void GivenPathToOriginalFileExpectedFullPath()
		{
			string pathToTestFile = "F:\\Studia\\SEMESTR_6\\BSK\\bsk_1\\README.MD";
			FileInfo testFile = new FileInfo(pathToTestFile);
			string expectedFullPath = "F:\\Studia\\SEMESTR_6\\BSK\\bsk_1\\README.MD";
			Assert.AreEqual(testFile.FullName, expectedFullPath);
		}
	}
}
