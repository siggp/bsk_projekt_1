﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Core.Cryptography
{
	// https://searchwindevelopment.techtarget.com/tip/Generate-RSA-public-and-private-keys-export-to-XML
	public class RsaGenerator
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="privateKey">File with private key</param>
		/// <param name="publicKey">File with public key</param>
		/// <param name="secureSize">Secure size is a length of desired key</param>
		public static void GenerateKeys(FileInfo privateKey, FileInfo publicKey, int secureSize)
		{
			FileStream keyFileStream = null;
			StreamWriter keyWriter = null;

			RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(secureSize);
			// private key
			try
			{
				keyFileStream = new FileStream(privateKey.FullName, FileMode.Create, FileAccess.Write);
				keyWriter = new StreamWriter(keyFileStream);

				keyWriter.Write(rsa.ToXmlString(true).ToString());
				keyWriter.Flush();
			}
			finally
			{
				if (keyWriter != null) keyWriter.Close();
				if (keyFileStream != null) keyFileStream.Close();
			}

			// public key
			try
			{
				keyFileStream = new FileStream(publicKey.FullName, FileMode.Create, FileAccess.Write);
				keyWriter = new StreamWriter(keyFileStream);
				keyWriter.Write(rsa.ToXmlString(false));
				keyWriter.Flush();
			}
			finally
			{
				if (keyWriter != null) keyWriter.Close();
				if (keyFileStream != null) keyFileStream.Close();
			}
			rsa.Clear();
		}

		public static string getKeyFromXml(FileInfo fileKey)
		{
			XmlDocument doc = new XmlDocument();
			doc.Load(fileKey.FullName);
			return doc.InnerXml;
		}

		public static string Encrypt(string textToEncrypt, string publicKeyString)
		{
			var bytesToEncrypt = Encoding.UTF8.GetBytes(textToEncrypt);

			using (var rsa = new RSACryptoServiceProvider(2048))
			{
				try
				{
					rsa.FromXmlString(publicKeyString.ToString());
					var encryptedData = rsa.Encrypt(bytesToEncrypt, true);
					var base64Encrypted = Convert.ToBase64String(encryptedData);
					return base64Encrypted;
				}
				finally
				{
					rsa.PersistKeyInCsp = false;
				}
			}
		}

		public static string Decrypt(string textToDecrypt, string privateKeyString)
		{
			var bytesToDescrypt = Encoding.UTF8.GetBytes(textToDecrypt);

			using (var rsa = new RSACryptoServiceProvider(2048))
			{
				try
				{

					// server decrypting data with private key                    
					rsa.FromXmlString(privateKeyString);

					var resultBytes = Convert.FromBase64String(textToDecrypt);
					var decryptedBytes = rsa.Decrypt(resultBytes, true);
					var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
					return decryptedData.ToString();
				}
				finally
				{
					rsa.PersistKeyInCsp = false;
				}
			}
		}

		public static void encryptPrivateKey(FileInfo privateKeyFile, FileInfo encryptedKeyFile, string userPassword)
		{
			string encryptedUserPassword = SHAKeyGenerator.GenerateSHA256(userPassword);
			AES keyEncryptor = new AES(BlockCiphersMode.CBC, 128, 128);
			var fileBytes = keyEncryptor.EncryptFile(encryptedUserPassword, privateKeyFile);
            File.WriteAllBytes(encryptedKeyFile.FullName, fileBytes);
		}

		public static void decryptPrivateKey(FileInfo encryptedKeyFile, FileInfo decryptedKeyFile, string userPassword)
		{
			string encryptedUserPassword = SHAKeyGenerator.GenerateSHA256(userPassword);
			AES keyEncryptor = new AES(BlockCiphersMode.CBC, 128, 128);
            var encryptedBytes = 
                keyEncryptor.DecryptFile(File.ReadAllBytes(encryptedKeyFile.FullName), encryptedUserPassword);
            File.WriteAllBytes(decryptedKeyFile.FullName, encryptedBytes);
		}
	}
}
