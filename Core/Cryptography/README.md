# Servwer App

## Encrypted files

Encrypted files directory after encryption you can modify on `Constants.cs` class

# RSA

## About

Keep it mind that we used XML files to keeping both files in different catalogs.

## Generating

To generate pair RSA keys (public key and private key) and save into XML file you'll need method `RsaGenerator.GenerateKeys(FileInfo privateKey, FileInfo publicKey, int secureSize)`

`sourceSize`- RSA keys length
`publicKey` - desired file with public key
`privateKey` - desired file with private key

## Encrypting some text using public Key

If you want encrypt some text (e.g session key) use `RsaGenerator.Encrypt(string textToEncrypt, string publicKeyString)`

`textToEncrypt` - text which you want to encrpyt
`publicKeyString` - public key to encrpytion

## Encrypting private key

To encrypt private key you need to use `RsaGenerator.encryptPrivateKey(FileInfo privateKeyFile, FileInfo encryptedKeyFile, string userPassword);`

## Decrypting private key

To decrypt private key you need to use `RsaGenerator.decryptPrivateKey(FileInfo encryptedKeyFile, FileInfo decryptedKeyFile, string userPassword);`

## Example

```
// Generating RSA keys
FileInfo myPublivKey = new FileInfo('F:/publicKey.xml');
FileInfo myPrivateKey = new FileInfo('F:/privateKey.xml');
RsaGenerator.GenerateKeys(myPrivateKey, myPublicKey, 2048);

// Encrypting private key
FileInfo myEncryptedPrivateKey = new FileInfo('F:/encryptedKeyFile.aes');
RsaGenerator.encryptPrivateKey(myPrivateKey, encryptedPrivateKey, 'userPassword');

// Decrypting private key
FileInfo decryptedPrivateKey = new FileInfo('F:/decryptedKeyFile.xml');
RsaGenerator.encryptPrivateKey(myEncryptedPrivateKey, decryptedPrivateKey, 'userPassword');
```

# Session Key

## Generating

To Generate Session key we use Guid. It's unique based on several properties technique. If you want to generate use `using Core.Cryptography` session key type `SessionKey.generate()`

## Encrypting session key

To encrypt session key you'll need static method `SessionKey.encryptSessionKey(Guid sessionKey, FileInfo publicKeyFile)`

## Example

```
// Generating session key
Guid mySessionKey = SessionKey.generate();

// Encrypting session key
FileInfo myPublicKey = new FileInfo('F:/publicKey.xml');
string encryptedSessionKey = SessionKey.encryptSessionKey(mySessionKey, myPublicKey)

// Decrypting session key
FileInfo myEncryptedPrivateKey = new FileInfo('F:/privateKey.aes');
string decryptedSessionKey = RsaGenerator.decryptSessionKey(encryptedSessionKey, )
```
