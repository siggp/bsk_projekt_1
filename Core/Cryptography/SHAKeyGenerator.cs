﻿using System.Text;
using System.Security.Cryptography;

namespace Core.Cryptography
{
	// https://codeshare.co.uk/blog/sha-256-and-sha-512-hash-examples/
	public static class SHAKeyGenerator
	{
		public static string GenerateSHA256(string inputText)
		{
			SHA256 sha256 = SHA256Managed.Create();
			byte[] bytes = Encoding.UTF8.GetBytes(inputText);
			byte[] hash = sha256.ComputeHash(bytes);
			return GetStringFromHash(hash);
		}

		private static string GetStringFromHash(byte[] hash)
		{
			StringBuilder result = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				result.Append(hash[i].ToString("X2"));
			}
			return result.ToString();
		}
	}
}
