﻿using System;
using System.IO;

namespace Core.Cryptography
{
	/// <summary>
	/// This class is responsible for Generating Session key and encrypt it using RSA. To generate session key we use GUIG mechanism
	/// To encrypt Session keys used https://gist.github.com/gashupl/27e4de6bd8f021f3d61b3122e6bbf775
	/// </summary>
	public class SessionKey
	{
		public static Guid generate()
		{
			return Guid.NewGuid();
		}

		public static string encryptSessionKey(Guid sessionKey, FileInfo publicKeyFile)
		{
			string publicKey = RsaGenerator.getKeyFromXml(publicKeyFile);
			return RsaGenerator.Encrypt(sessionKey.ToString(), publicKey);
		}

		public static string decryptSessionKey(string encryptedSessionKey, FileInfo privateKeyFile)
		{
			string privateKey = RsaGenerator.getKeyFromXml(privateKeyFile);
			return RsaGenerator.Decrypt(encryptedSessionKey, privateKey);
		}
	}
}
