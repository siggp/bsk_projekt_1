﻿namespace Core.Cryptography
{
    public enum BlockCiphersMode
    {
        ECB,
        CBC,
        CFB,
        OFB
    }
}
