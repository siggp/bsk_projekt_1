﻿namespace Core.Cryptography
{
    public class BlockSizesProvider
    {
        private BlockCiphersMode mode;

        public BlockSizesProvider(BlockCiphersMode mode)
        {
            this.mode = mode;
        }

        public int[] Provide()
        {
            if (mode == BlockCiphersMode.CFB || mode == BlockCiphersMode.OFB)
            {
                return new int[] { 128, 192, 254 };
            } else
            {
                return new int[] { 128 };
            }
        }
    }
}
