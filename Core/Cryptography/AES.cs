﻿using System.Security.Cryptography;
using System.IO;
using System;
using Core.ViewModels;

namespace Core.Cryptography
{
	// https://ourcodeworld.com/articles/read/471/how-to-encrypt-and-decrypt-files-using-the-aes-encryption-algorithm-in-c-sharp
	public class AES
	{
		public int KeySize { get; private set; }
		public int BlockSize { get; private set; }	// number of bits
		public BlockCiphersMode AesCipherMode { get; set; }

		public AES(BlockCiphersMode cipherMode, int keySize, int blockSize)
		{
			this.AesCipherMode = cipherMode;
			this.BlockSize = blockSize;
			this.KeySize = keySize;
		}

		public byte[] EncryptFile(string password, FileInfo inputFile, bool deleteEncrptedFile = true, ServerViewModel server = null)
		{
            var file = File.Create(inputFile.Directory + "//" + Path.GetFileNameWithoutExtension(inputFile.FullName) + ".aes");
            var outputFile = new FileInfo(file.Name);
            file.Close();

			// http://stackoverflow.com/questions/27645527/aes-encryption-on-large-files
            byte[] salt = GenerateRandomSalt();
			// string outputEncryptedFilePathWithoutExtension = Path.GetDirectoryName(this.OutputFilePath) + "\\" + Path.GetFileName(this.InputFilePath);
			string outputEncryptedFilePathWithoutExtension = outputFile.FullName + "\\" + inputFile.Name;

			FileStream fileEncrpted = new FileStream(outputFile.FullName, FileMode.Create);	// TODO path to encryptedFile should not be hardcoded

			byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);

			RijndaelManaged aes = setAesWithParameters(passwordBytes, salt);
	
			// write salt to the begining of the output file, so in this case can be random every time
			fileEncrpted.Write(salt, 0, salt.Length);
	
			CryptoStream fileEncryptedCryptoStream = new CryptoStream(fileEncrpted, aes.CreateEncryptor(), CryptoStreamMode.Write);
			FileStream originalFile = new FileStream(inputFile.FullName, FileMode.Open);
	
			// create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
			byte[] buffer = new byte[1048576];
			int read;
			long encryptedMemory = 0; // value to calculate read memory

            byte[] encryptedFileInBytes = null;
            try
			{
				while ((read = originalFile.Read(buffer, 0, buffer.Length)) > 0)
				{
					encryptedMemory += read;
					if (server != null)
                    {
                        server.EncryptionProgress = (int)Math.Round(((float)encryptedMemory / inputFile.Length) * 100);
                    }
                    fileEncryptedCryptoStream.Write(buffer, 0, read);
				}                    
            }
			catch (Exception ex)
			{
				Console.WriteLine("Error: " + ex.Message);
			}
			finally
			{
				fileEncryptedCryptoStream.Close();
				fileEncrpted.Close();
                originalFile.Close();
                encryptedFileInBytes = File.ReadAllBytes(outputFile.FullName);

                if (deleteEncrptedFile)
                {
                    File.Delete(outputFile.FullName);
                }
            }
            if (server != null)
            {
                server.EncryptionStatus = OperationStatus.Finished;
            }
            return encryptedFileInBytes;
        }

        public byte[] DecryptFile(byte[] encryptFileBytes, string password, bool deleteEncrptedFile = true)
		{

            var file = File.Create("inputFile");
            var inputFile = new FileInfo(file.Name);
            file.Close();
            File.WriteAllBytes(inputFile.FullName, encryptFileBytes);
            
            file = File.Create("outputFile");
            var outputFile = new FileInfo(file.Name);
            file.Close();


            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
			byte[] salt = new byte[32];

			FileStream fsEncryptedFile = new FileStream(inputFile.FullName, FileMode.Open);
			fsEncryptedFile.Read(salt, 0, salt.Length);

			RijndaelManaged aes = setAesWithParameters(passwordBytes, salt);

			CryptoStream cryptoStreamDecryptor = new CryptoStream(fsEncryptedFile, aes.CreateDecryptor(), CryptoStreamMode.Read);

			FileStream fsDecryptedFile = new FileStream(outputFile.FullName, FileMode.Open);

			int read;
			byte[] buffer = new byte[1048576];

			try
			{
				while ((read = cryptoStreamDecryptor.Read(buffer, 0, buffer.Length)) > 0)
				{
					fsDecryptedFile.Write(buffer, 0, read);
				}
			}
			catch (CryptographicException ex_CryptographicException)
			{
				Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error: " + ex.Message);
			}
            byte[] encryptedFileInBytes = null;

            try
			{
				cryptoStreamDecryptor.Close();

                fsDecryptedFile.Close();
                fsEncryptedFile.Close();

                encryptedFileInBytes = File.ReadAllBytes(outputFile.FullName);
                if (deleteEncrptedFile)
                {
                    File.Delete(outputFile.FullName);
                    File.Delete(inputFile.FullName);
                }
            }
			catch (Exception ex)
			{
				Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
			}
			finally
			{
				fsDecryptedFile.Close();
				fsEncryptedFile.Close();
			}

            return encryptedFileInBytes;
        }

		private byte[] GenerateRandomSalt()
		{
			byte[] randomData = new byte[32];

			using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
			{
				for (int i = 0; i < 10; i++)
				{
					rng.GetBytes(randomData);
				}
			}
			return randomData;
		}

		// http://stackoverflow.com/questions/2659214/why-do-i-need-to-use-the-rfc2898derivebytes-class-in-net-instead-of-directly
		// "What it does is repeatedly hash the user password along with the salt." High iteration counts.
		private Rfc2898DeriveBytes hashUserPassword(byte[] passwordBytes, byte[] salt)
		{
			return new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
		}
		
		private RijndaelManaged setAesWithParameters(byte[] passwordBytes, byte[] salt)
		{
			RijndaelManaged aes = new RijndaelManaged();
			aes.KeySize = this.KeySize;
			aes.BlockSize = this.BlockSize;
			var encryptionKey = hashUserPassword(passwordBytes, salt);

			aes.Key = encryptionKey.GetBytes(aes.KeySize / 8);
			aes.IV = encryptionKey.GetBytes(aes.BlockSize / 8);
			aes.Padding = PaddingMode.PKCS7;

			switch (this.AesCipherMode)
			{
				case BlockCiphersMode.CBC:
					aes.Mode = CipherMode.CBC;
					break;
				case BlockCiphersMode.ECB:
					aes.Mode = CipherMode.ECB;
					break;
				case BlockCiphersMode.CFB:
					aes.Mode = CipherMode.CFB;
					break;
				case BlockCiphersMode.OFB:
					aes.Mode = CipherMode.OFB;
					break;
				default:
					aes.Mode = CipherMode.CBC;
					break;
			}

			return aes;
		}
	}
}
