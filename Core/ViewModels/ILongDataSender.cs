﻿namespace Core.ViewModels
{
    public interface ILongDataSender
    {
        int SendingProgress { get; set; }
    }
}
