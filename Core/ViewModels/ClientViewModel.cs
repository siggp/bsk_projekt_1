﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace Core.ViewModels
{
    public class ClientViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public bool IsLoginEnabled { get; set; } = false;
        public bool IsSaveEnabled { get; set; } = false;
        public bool IsLoggedIn { get; set; } = false;
        public bool IsSaved { get; set; } = false;
        public string DirectoryName { get { return Directory != null ? Directory.Name : "No file choosed"; } }
        public string IpAddress { get; set; }
        public bool IsNotConnected { get; set; } = true;

        private string userName;
        public string UserName
        {
            get { return userName; }
            set
            {
                this.userName = value;
                IsLoginEnabled = UserName != null && Password != null;
                OnPropertyChanged("IsLoginEnabled");
            }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                this.password = value;
                IsLoginEnabled = UserName != null && Password != null;
                OnPropertyChanged("IsLoginEnabled");
            }
        }

        private List<string> users;
        public List<string> Users
        {
            get { return users; }
            set
            {
                this.users = value;
                OnPropertyChanged();
            }
        }

        private OperationStatus downloadStatus = OperationStatus.NotStarted;
        public OperationStatus DownloadStatus
        {
            get { return downloadStatus; }
            set
            {
                this.downloadStatus = value;
                OnPropertyChanged();

                IsSaveEnabled = Directory != null && NewName != null && DownloadStatus == OperationStatus.Finished;
                OnPropertyChanged("IsSaveEnabled");
            }
        }

        private OperationStatus saveStatus = OperationStatus.NotStarted;
        public OperationStatus SaveStatus
        {
            get { return saveStatus; }
            set
            {
                this.saveStatus = value;
                OnPropertyChanged();
            }
        }

        private string originalName = "";
        public string OriginalName
        {
            get { return originalName; }
            set
            {
                originalName = value;
                OnPropertyChanged();
            }
        }

        private string extension = "";
        public string Extension
        {
            get { return extension; }
            set
            {
                extension = value;
                OnPropertyChanged();
            }
        }

        private string newName = "";
        public string NewName
        {
            get { return newName; }
            set
            {
                newName = value;
                OnPropertyChanged();

                IsSaveEnabled = Directory != null && NewName != null && DownloadStatus == OperationStatus.Finished;
                OnPropertyChanged("IsSaveEnabled");
            }
        }

        private DirectoryInfo directory;
        public DirectoryInfo Directory
        {
            get { return directory; }
            set
            {
                this.directory = value;
                OnPropertyChanged();
                OnPropertyChanged("DirectoryName");

                IsSaveEnabled = Directory != null && NewName != null && DownloadStatus == OperationStatus.Finished;
                OnPropertyChanged("IsSaveEnabled");
            }
        }

        private bool isSelectionEnable = true;
        public bool IsSelectionEnable
        {
            get { return isSelectionEnable; }
            set
            {
                isSelectionEnable = value;
                OnPropertyChanged();
                if (!value)
                {
                    IsLoginEnabled = false;
                    OnPropertyChanged("IsLoginEnabled");
                }
            }
        }

        private bool isConnected = false;
        public bool IsConnected
        {
            get { return isConnected; }
            set
            {
                isConnected = value;
                IsNotConnected = !value;
                OnPropertyChanged("IsNotConnected");
            }
        }

        public void OnPropertyChanged([CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }
    }
}
