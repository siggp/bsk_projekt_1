﻿namespace Core.ViewModels
{
    public enum OperationStatus
    {
        NotStarted,
        Processing,
        Finished
    }
}
