﻿using Core.Cryptography;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Net;

namespace Core.ViewModels
{
    public class ServerViewModel : INotifyPropertyChanged, ILongDataSender
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Array BlockCiphersModeValues => Enum.GetValues(typeof(BlockCiphersMode));
        public int[] BlockSizes => new BlockSizesProvider(Mode).Provide();

        public string FileName { get { return File != null ? File.Name : "No file choosed"; } }
        public bool IsEnabled { get; set; } = false;
        public string IpAddress { get; set; } = Dns.GetHostEntry(Dns.GetHostName())
            .AddressList
            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            .ToString();

        private int blockSize;
        public int BlockSize
        {
            get
            {
                if (blockSize == 0)
                {
                    blockSize = BlockSizes.FirstOrDefault();
                    OnPropertyChanged();
                }
                return blockSize;
            }
            set
            {
                this.blockSize = value;
                OnPropertyChanged();
            }
        }

        private BlockCiphersMode mode = BlockCiphersMode.ECB;
        public BlockCiphersMode Mode{
            get { return mode; }
            set
            {
                mode = value;
                if (!new BlockSizesProvider(value).Provide().Contains(BlockSize))
                {
                    BlockSize = BlockSizes.First();
                    OnPropertyChanged("BlockSize");
                }
                OnPropertyChanged("BlockSizes");
            }
        }

        private OperationStatus sendingStatus = OperationStatus.NotStarted;
        public OperationStatus SendingStatus
        {
            get { return sendingStatus; }
            set
            {
                sendingStatus = value;
                OnPropertyChanged();
            }
        }

        private int sendingProgress = 0;
        public int SendingProgress
        {
            get { return sendingProgress; }
            set
            {
                sendingProgress = value;
                OnPropertyChanged();
            }
        }

        private OperationStatus encryptionStatus = OperationStatus.NotStarted;
        public OperationStatus EncryptionStatus
        {
            get { return encryptionStatus; }
            set
            {
                encryptionStatus = value;
                OnPropertyChanged();
            }
        }


        private int encryptionProgress = 0;
        public int EncryptionProgress
        {
            get { return encryptionProgress; }
            set
            {
                encryptionProgress = value;
                OnPropertyChanged();
            }
        }

        private FileInfo file;
        public FileInfo File {
            get { return file; }
            set
            {
                file = value;
                IsEnabled = value != null ? true : false;
                OnPropertyChanged("FileName");
                OnPropertyChanged("IsEnabled");
            }
        }

        private bool isSelectionEnable = true;
        public bool IsSelectionEnable
        {
            get { return isSelectionEnable; }
            set
            {
                isSelectionEnable = value;
                OnPropertyChanged();
                if (!value)
                {
                    IsEnabled = false;
                    OnPropertyChanged("IsEnabled");
                }
            }
        }

        public void OnPropertyChanged([CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }

        public void Reset()
        {
            EncryptionProgress = 0;
            EncryptionStatus = OperationStatus.NotStarted;
            SendingProgress = 0;
            SendingStatus = OperationStatus.NotStarted;
        }
    }
}