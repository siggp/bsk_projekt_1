﻿using Core.Cryptography;

namespace Core.DTO
{
    public class Metadata
    {
        public string SessionKey { get; set; }
        public string OriginalName { get; set; }
        public string Extension { get; set; }
        public BlockCiphersMode BlockCiphersMode { get; set; }
        public int BlockSize { get; set; }
    }
}
