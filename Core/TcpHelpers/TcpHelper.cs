﻿using Core.ViewModels;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Core.TcpHelpers
{
    public class TcpHelper
    {
        private readonly NetworkStream stream;

        public TcpHelper(NetworkStream stream)
        {
            this.stream = stream;
        }

        public async Task<string> ReceiveString()
        {
            return Encoding.UTF8.GetString(await ReceiveBytes());
        }

        public async Task<Byte[]> ReceiveBytes()
        {
            Byte[] size = new Byte[4];
            await stream.ReadAsync(size, 0, size.Length);
            Byte[] buffer = new Byte[BitConverter.ToInt32(size, 0)];

            if (buffer.Length > 1000)
            {
                int read = 0;
                while (read != buffer.Length)
                {
                    int length = buffer.Length - read > 100 ? 100 : buffer.Length - read;
                    await stream.ReadAsync(buffer, read, length);
                    read += length;
                }
            } else
            {
                await stream.ReadAsync(buffer, 0, buffer.Length);
            }
            return buffer;
        }

        public async Task SendString(string message)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            await SendBytes(buffer);
        }

        public async Task SendBytes(Byte[] buffer, ILongDataSender sender = null)
        {
            setProgress(sender, 0);
            Byte[] size = BitConverter.GetBytes(buffer.Length);
            await stream.WriteAsync(size, 0, size.Length);
            if (buffer.Length > 1000)
            {
                int write = 0;
                while (write != buffer.Length)
                {
                    int length = buffer.Length - write > 100 ? 100 : buffer.Length - write;
                    await stream.WriteAsync(buffer, write, length);
                    write += length;
                    setProgress(sender, ((float)write / buffer.Length) * 100);
                }
            }
            else
            {
                await stream.WriteAsync(buffer, 0, buffer.Length);
            }
            setProgress(sender, 100);
        }

        private void setProgress(ILongDataSender sender, float progress)
        {
            if (sender != null)
            {
                sender.SendingProgress = (int)Math.Round(progress);
            }
        }
    }
}