﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
	class User
	{
		public string Email { get; set; }
		public string Password { get; set; }
		public string PublicKeyPath { get; set; }
		public string PrivateKeyPath { get; set; }

		public User(string email, string password)
		{
			this.Email = email;
			this.Password = password;
		}
	}
}
