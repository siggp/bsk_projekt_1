﻿using Core.Cryptography;
using Core.TcpHelpers;
using Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Core.DTO;
using System.Windows.Forms;

namespace Core
{
    public class ServerManager
    {
        private readonly ServerViewModel viewModel;
        private readonly AES aes;
        private TcpListener server;
        private List<string> users;

        public ServerManager(string ip,
            int port,
            ServerViewModel serverViewModel)
        {
            try
            {
                server = new TcpListener(IPAddress.Parse(ip), port);    
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            this.viewModel = serverViewModel;
            this.aes = new AES(viewModel.Mode, 256, viewModel.BlockSize);

            users = new List<string>();
            using (var stream = new StreamReader("../../../RSA-Keys/users.txt"))
            {
                while (true)
                {
                    var user = stream.ReadLine();
                    if (user != null)
                    {
                        users.Add(user);
                    } else
                    {
                        break;
                    }
                }
            }
        }

        public async void Start()
        {
            try
            {
                server.Start();
                while(true)
                {
                    viewModel.Reset();
                    var client = await server.AcceptTcpClientAsync();
                    Console.WriteLine("New client connected");

                    using (NetworkStream stream = client.GetStream())
                    {
                        var tcpHelper = new TcpHelper(stream);
                        await tcpHelper.SendString(JsonConvert.SerializeObject(users));
                        var user = await tcpHelper.ReceiveString();

                        var sessionKey = SessionKey.generate();
                        await tcpHelper
                            .SendString(JsonConvert.SerializeObject(getMetadata(viewModel, user, sessionKey)));
                        viewModel.SendingStatus = OperationStatus.Processing;
                        viewModel.EncryptionStatus = OperationStatus.Processing;
                        await tcpHelper.SendBytes(aes.EncryptFile(sessionKey.ToString(), viewModel.File, true, viewModel), viewModel);
                    }
                    client.Close();
                    viewModel.SendingStatus = OperationStatus.Finished;
                    MessageBox.Show("The client was served. Server is waiting for new connection");
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                server.Stop();
            }
        }

        private Metadata getMetadata(ServerViewModel viewModel, string user, Guid sessionKey)
        {
            
            var publicKey = new FileInfo("../../../RSA-Keys/Public/public" + user + ".xml");
            return new Metadata()
            {
                SessionKey = SessionKey.encryptSessionKey(sessionKey, publicKey),
                OriginalName = Path.GetFileNameWithoutExtension(viewModel.FileName),
                Extension = viewModel.File.Extension,
                BlockCiphersMode = viewModel.Mode,
                BlockSize = viewModel.BlockSize
            };
        }
    }
}