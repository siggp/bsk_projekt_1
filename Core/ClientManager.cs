﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using Core.Cryptography;
using Core.DTO;
using Core.TcpHelpers;
using Core.ViewModels;
using Newtonsoft.Json;

namespace Core
{
    public class ClientManager
    {
        private ClientViewModel viewModel;

        public async void StartConnection(string server,
            int port,
            ClientViewModel viewModel)
        {
            this.viewModel = viewModel;
            try
            {
                viewModel.IsConnected = true;
                TcpClient client = new TcpClient();
                await client.ConnectAsync(server, port);
                using (var stream = client.GetStream())
                {
                    var tcpHelper = new TcpHelper(stream);

                    viewModel.Users = JsonConvert
                        .DeserializeObject<List<string>>(await tcpHelper.ReceiveString());

                    await waitForLogIn();

                    await tcpHelper.SendString(viewModel.UserName);

                    var metadata = JsonConvert
                        .DeserializeObject<Metadata>(await tcpHelper.ReceiveString());

                    mapMetadataToViewModel(viewModel, metadata);

                    var sessionKey = getDecryptedSessionKey(metadata.SessionKey);

                    var aes = new AES(metadata.BlockCiphersMode, 256, metadata.BlockSize);
                    viewModel.DownloadStatus = OperationStatus.Processing;

                    var encrypedFile = await tcpHelper.ReceiveBytes();
                    var decrypedFile = aes.DecryptFile(encrypedFile, sessionKey);
                    viewModel.DownloadStatus = OperationStatus.Finished;

                    await waitForSaveButtonClick();

                    viewModel.SaveStatus = OperationStatus.Processing;
                    var file = File.Create(viewModel.Directory.FullName + "//" + viewModel.NewName + metadata.Extension);
                    file.Close();
                    File.WriteAllBytes(file.Name, decrypedFile);
                    viewModel.SaveStatus = OperationStatus.Finished;
                }

                client.Close();
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
                viewModel.IsConnected = false;
             }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                viewModel.IsConnected = false;
            }
        }

        private async Task waitForLogIn()
        {
            while (true)
            {
                if (viewModel.IsLoggedIn)
                {
                    break;
                } else
                {
                    await Task.Delay(100);
                }
            }
        }

        private async Task waitForSaveButtonClick()
        {
            while (true)
            {
                if (viewModel.IsSaved)
                {
                    break;
                }
                else
                {
                    await Task.Delay(100);
                }
            }
        }

        private void mapMetadataToViewModel(ClientViewModel viewModel, Metadata metadata)
        {
            viewModel.OriginalName = metadata.OriginalName;
            viewModel.Extension = metadata.Extension;
        }

        private string getDecryptedSessionKey(string encriptedSessionKey)
        {
            var privateKeyFile = File.Create("privateKey.xml");
            privateKeyFile.Close();
            var privateKeyFileInfo = new FileInfo(privateKeyFile.Name);
            var encryptedKeyFile = new FileInfo("../../../RSA-Keys/Private/encryptedPrivate" + viewModel.UserName + ".aes");
            RsaGenerator.decryptPrivateKey(encryptedKeyFile, privateKeyFileInfo, viewModel.Password);
            var sessionKey = SessionKey.decryptSessionKey(encriptedSessionKey, privateKeyFileInfo);
            File.Delete(privateKeyFileInfo.FullName);
            return sessionKey;
        }
    }
}
