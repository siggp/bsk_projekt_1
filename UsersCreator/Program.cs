﻿using Core.Cryptography;
using System;
using System.IO;

namespace UsersCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("User name: ");
            var userName = Console.ReadLine();

            Console.WriteLine("Password: ");
            var password = Console.ReadLine();

            // Generate Keys
            var privateKeyFile = File.Create("../../../RSA-Keys/Private/private.xml");
            var publicKeyFile = File.Create("../../../RSA-Keys/Public/public" + userName + ".xml");
            var encryptedPrivateKeyFile = File.Create("../../../RSA-Keys/Private/encryptedPrivate" + userName + ".aes");

            var privateKeyFileInfo = new FileInfo(privateKeyFile.Name);
            var publicKeyFileInfo = new FileInfo(publicKeyFile.Name);
            var encryptedPrivateKeyFileInfo = new FileInfo(encryptedPrivateKeyFile.Name);

            privateKeyFile.Close();
            publicKeyFile.Close();
            encryptedPrivateKeyFile.Close();

            RsaGenerator.GenerateKeys(privateKeyFileInfo, publicKeyFileInfo, 2048);
            RsaGenerator.encryptPrivateKey(privateKeyFileInfo, encryptedPrivateKeyFileInfo, password);
            File.Delete(privateKeyFileInfo.FullName);

            // Save user
            using (StreamWriter stream = File.AppendText("../../../RSA-Keys/users.txt"))
            {
                stream.WriteLine(userName);
            }
        }
    }
}
