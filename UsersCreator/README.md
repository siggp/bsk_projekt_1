﻿# How it works
1. In root directory there is a "RSA-Keys" folder
2. Run console app
3. Type the name of a new user
4. Type password for the new user
5. The user will be added to RSA-Keys/users.txt file
6. For the user new public and privte keys will be generated in folders `Public and Private`. The user name will be a sufix of the file name.