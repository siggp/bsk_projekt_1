# How to use
### 1. Open BSK_1.sln and build all projects
### 2. Next you have to create some users
- Go to directory: `BSK_1/UsersCreator/bin/Debug`
- Run a console application: `UsersCreator.exe`
- Type user name and press `enter`
- Type password for the user and press `enter`
- The new user will be created
- To create more users run `UsersCreator.exe` app more times
### 3. Run the server WPF application
- Go to directory: `BSK_1/Server/bin/Debug`
- Run a WPF application: `Server.exe`
- In title you can find server ip address
- Select a file which you would like to encrypt
- Select mode of block ciphers
- Select sub-block length
- Click `Encrypt the file and start listening for the clients`
- The server will be waiting for new clients connections and serve them sequentially
### 4. Run the client WPF application
- Go to directory: `BSK_1/Client/bin/Debug`
- Run a WPF application: `Client.exe`
- Type server ip address and click connect
- Select your user name
- Type your password (which you have typed while creating the user)
- Click `Login and download encrypted file`
- Wait untill status in `Download Encrypted File Panel` will we set to `Finished`
- Type new file name
- Select directory where to save decrypted file
- Click `Save` button. The file will be saved and the app will exit