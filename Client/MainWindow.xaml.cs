﻿using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Core;
using Core.ViewModels;


namespace bsk_1etap_client
{
    public partial class MainWindow : Window
    {
        public ClientViewModel viewModel { get; set; }
        public ClientManager clientManager { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            viewModel = new ClientViewModel();
            DataContext = viewModel;
        }

        private void Connect(object sender, RoutedEventArgs e)
        {
            var clientManager = new ClientManager();
            clientManager.StartConnection(viewModel.IpAddress, 5478, viewModel);
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            viewModel.IsLoggedIn = true;
            viewModel.IsSelectionEnable = false;
        }

        private async void Save(object sender, RoutedEventArgs e)
        {
            viewModel.IsSaved = true;
            while (viewModel.SaveStatus != OperationStatus.Finished)
            {
                await Task.Delay(500);
            }
            System.Windows.Application.Current.Shutdown();
        }

        private void ChooseDirectory(object sender, RoutedEventArgs e)
        {
            var directoryChooser = new FolderBrowserDialog();
            viewModel.Directory = directoryChooser.ShowDialog() == System.Windows.Forms.DialogResult.OK
                ? new DirectoryInfo(directoryChooser.SelectedPath) : null;
        }

        private void OnPasswordChanged(
            object sender,
            RoutedEventArgs e)
        {
            viewModel.Password = passwordBox.Password;
        }
    }
}
