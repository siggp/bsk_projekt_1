﻿using System.Windows;
using Microsoft.Win32;
using System.IO;
using Core.ViewModels;
using System.Threading;
using Core;
using System.Net;
using System.Linq;

namespace Server
{
	public partial class MainWindow : Window
	{
        public ServerViewModel viewModel { get; set; }
        public MainWindow()
		{
		    InitializeComponent();
            viewModel = new ServerViewModel();
			DataContext = viewModel;
		}

        private void ChooseFile(object sender, RoutedEventArgs e)
        {
            var fileChooser = new OpenFileDialog();
            viewModel.File = fileChooser.ShowDialog() == true
                ? new FileInfo(fileChooser.FileName) : null;
        }

        private void StartServer(object sender, RoutedEventArgs e)
        {
            viewModel.IsSelectionEnable = false;

            var serverManager = new ServerManager(viewModel.IpAddress, 5478, viewModel);

            var connectionThread = new Thread(serverManager.Start);
            connectionThread.Start();
        }
    }
}